<?php get_template_part('parts/header'); the_post(); ?>

<?php get_template_part('parts/page', 'header'); ?>

<main>

  <section class="padding--bottom">
    <div class="wrap white--bg bx-shadow bx-shadow--purple single__container">

      <article id="post-<?php the_ID(); ?>"
               <?php post_class(); ?>
               itemscope itemtype="http://schema.org/BlogPosting"> 

        <div itemprop="articleBody" class="single__content">

          <?php the_content(); ?>
          
        </div>

      </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>