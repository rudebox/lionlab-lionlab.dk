<?php get_template_part('parts/header'); ?>
<?php get_template_part('parts/archive', 'header'); ?>

<main>

  <section class="padding--both archive">
    <div class="wrap hpad clearfix">


    <?php 
      // Query Arguments
      $args = array(
        'posts_per_page' => -1,
        'order' => 'DESC',
        'cat' => 9,
      );

      // The Query
      $query = new WP_Query( $args );
    ?>

    <div class="cases__row flex flex--wrap">

      <?php if ($query->have_posts()): ?>
        <?php while ($query->have_posts()): $query->the_post(); ?>

        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog' );?>


        <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" class="<?php echo $class; ?> cases__post cases__post--home cases__post--blog fourcol" itemscope itemtype="http://schema.org/BlogPosting">
            
          <div class="cases__content-wrap">
            <header>
              <img src="<?php echo $thumb['0']; ?>" alt="blog_indlaeg">
            </header>

            <h2 class="cases__post--title cases__post--title--archive h4">
                <?php the_title(); ?>
            </h2>

            <div class="cases__post--excerpt">
              <?php echo the_excerpt(); ?>
            </div>

          </div>

        </a>

        <?php endwhile; 
         wp_reset_postdata(); 
        else: ?>

          <p>No posts here.</p>

      <?php endif; ?>

    </div>
    </div> 

  </section>

</main>

<?php get_template_part('parts/footer'); ?>