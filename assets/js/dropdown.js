jQuery(document).ready(function($) {
	$('ul.main-nav').find('> li').click(
	    function() {
	        $(this).find('> ul').slideToggle();
	    }
	);

	$('ul.sub-menu').find('> li').click(
	    function(e) {
	        e.stopPropagation()
	        $(this).find('> ul').slideToggle();
	    }
	);
});