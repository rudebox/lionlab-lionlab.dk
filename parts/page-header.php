<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}
?>

<?php
  //hero field group
  $img_home = get_field('page_img', get_option('page_for_posts') );
  $img = get_field('page_img');
  $text = get_field('page_text');
  $text_home = get_field('page_text', get_option('page_for_posts') );
  $link = get_field('page_link'); 
  $link_home = get_field('page_link', get_option('page_for_posts') ); 
?>

<section class="hero purple--bg">
  <div class="wrap clearfix hpad">
      <div class="row">

        <div class="sixcol">
          <h1 class="hero__title"><?php echo $title; ?></h1>
          <?php if (!is_home()) : ?>
          <?php echo $text; ?>
          <?php else: ?>
          <?php echo $text_home; ?>
      	  <?php endif; ?>

          <?php if ($link) : ?>
          <a href="<?php echo $link; ?>" class="btn btn--gradient">Se mere</a>
      	  <?php endif; ?>

      	  <?php if (is_single() ) : ?>
      	  	 <a class="btn btn--gradient btn--back" href="javascript: history.go(-1)">Tilbage</a>
      	  <?php endif; ?>
        </div>

		<?php if (!is_home()) : ?>
			<?php if ($img) : ?>
	        <div class="sixcol hero__img">
	          <img src="<?php echo $img['url']; ?>" alt="hero">
	        </div>
	    	<?php endif; ?>
	        <?php else: ?>
	        <?php if ($img_home) : ?>
	        <div class="sixcol hero__img">
	          <img src="<?php echo $img_home['url']; ?>" alt="hero">
	        </div>
	        <?php endif; ?>
    	<?php endif; ?>
    </div>
  </div>
</section>
