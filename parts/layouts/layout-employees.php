<?php 
	//layout type: employees repeater field group

	if (have_rows('employee') ) : 
 ?>

 <section class="employees padding--both">
 	<div class="hpad clearfix wrap">
 		<div class="row flex flex--wrap">
	 		<?php while (have_rows('employee') ) : the_row(); 
	 			$img = get_sub_field('employee_img');
	 			$name = get_sub_field('employee_name');
	 			$position = get_sub_field('employee_position');
	 			$phone = get_sub_field('employee_phone');
	 			$mail = get_sub_field('employee_mail');
	 		?>
	 		<div class="fourcol employees__item">
	 			<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">	
 				<h4 class="employees__title"><?php echo $name; ?></h4>
 				<?php echo $position; ?>

 				<div class="employees__wrap">
 					<?php if ($phone) : ?>
		 			<a class="employees__phone" href="tel:<?php echo get_formatted_phone($phone); 
		 			?>"><?php echo $phone; ?></a>
		 			<?php endif; ?>
		 			<?php if ($mail) : ?>
		 			<a class="employees__mail" href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a>
		 			<?php endif; ?>
	 			</div>	 				
	 		</div>
 		<?php endwhile; ?>
 		</div>
 	</div>
 </section>
 <?php endif; ?>