<?php 
	//layout type: split section field group

	$title = get_sub_field('split_title');
	$text = get_sub_field('split_text');
	$image = get_sub_field('split_image');
	$position = get_sub_field('split_position');
	$bg = get_sub_field('split_bg');

	if ($position === 'right') {
		$pull = 'right';
	}

	if ($position === 'left') {
		$push = 'sixcol-offset';
		$left = 'left';
	}
 ?>

 <section class="split <?php echo $bg; ?>"> 
 	<div class="wrap hpad clearfix split__container">
 		<div class="row flex flex--wrap split__row">
	 		<div class="sixcol split__image <?php echo $pull; ?> <?php echo $left; ?>">
	 			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"> 
	 		</div>
	 		<div class="sixcol <?php echo $push; ?> split__text">
	 			<h2 class="split__title"><?php echo $title; ?></h2>
	 			<?php echo $text; ?>
	 		</div>
 		</div>
 	</div>
 </section>