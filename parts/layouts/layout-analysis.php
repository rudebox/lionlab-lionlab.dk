<?php 
	//layout type: analysis field group
	$img = get_sub_field('analyse_img');
 ?>

 <section class="analysis padding--both">
 	<div class="wrap clearfix hpad center">
 		<img src="<?php echo $img['sizes']['medium']; ?>" alt="<?php echo $img['alt']; ?>">
 		<div class="tvwelvecol gradient-tb--bg center analysis__box bx-shadow--purple bx-shadow">
 			<h3 class="analysis__title"><?php echo the_sub_field('analyse_title'); ?></h3>
 			<p><?php echo the_sub_field('analyse_text'); ?></p>
 			<div class="center">
 			  <?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true );
 			  ?>
 			 </div>
 		</div>
 	</div>
 </section>