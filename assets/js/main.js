jQuery(document).ready(function($) {

  console.log('Outsource-dk suck our balls!'); 

  //owl slider/carousel
  var owl = $(".cases__track");

  owl.owlCarousel({
      loop: false,
      margin: 30,
      items: 4,
      autoplay: false,
      dots: false,
      nav: true,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"],
      responsiveClass: true,
      responsive : {
      // breakpoint from 0 up
      0 : {
          items: 1,
          nav: true
      },
      // breakpoint from 480 up
      480 : {
          items: 2,
          nav: true
      },
      // breakpoint from 768 up
      768 : {
          items: 3,
          nav: true
      },
      // breakpoint from 768 up
      900 : {
          items: 4,
          nav: true
      }
    }
  });


  //preloader 
  if($('body').hasClass('home') ) { 
  var Body = $('body');
    Body.addClass('preloader-site is-fixed');
    
    $(window).load(function(){

      setTimeout(function() { 
        $('.preloader__wrapper').fadeOut();
        $('body').removeClass('preloader-site is-fixed');
          }, 1900);

    });
  }


  //avoid Adblock interaction because of name conflict
  if($('body').hasClass('google-ads') ) {
    var Body = $('body');
    Body.removeClass('google-ads');
  }


  //remove l sep characters
  $(".wysiwygs__col").children().each(function() {
    $(this).html($(this).html().replace(/&#8232;/g," "));
  }); 


  //active menu link
  $(".current-menu-item a").each(function() {   
      if (this.href == window.location.href) {
          $(this).addClass("is-active");
      }
  });


  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
    $('.close-menu').toggleClass('is-fixed');

    $(".nav-toggle__label").fadeOut(function () {
      $(".nav-toggle__label").text(($(".nav-toggle__label").text() == 'Menu') ? 'Luk' : 'Menu').fadeIn();
    })
  });

  $('.close-menu').click(function(e) {
    e.preventDefault();
    $('.close-menu').toggleClass('is-closed');
    $('.close-menu').removeClass('is-fixed');
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.main-nav').slideUp();

    $(".nav-toggle__label").fadeOut(function () {
      $(".nav-toggle__label").text(($(".nav-toggle__label").text() == 'Menu') ? 'Luk' : 'Menu').fadeIn();
    })
  });


  //mixit up
  if($('body').is('.blog')){

    // Instantiate and configure the mixer
    var mixer = mixitup('.mixit', {

    });

  }


  //page transition
   $(".animsition").animsition({ 
      inClass: 'fade-in',
      outClass: 'fade-out',
      inDuration: 2000,
      outDuration: 800,
      linkElement: '.animsition-link',
      // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
      loading: false,
      loadingParentElement: 'html', //animsition wrapper element
      loadingClass: 'animsition-loading',
      loadingInner: '', // e.g '<img src="loading.svg" />' 
      timeout: false,
      timeoutCountdown: 5000,
      onLoadEvent: true,
      browser: [ 'animation-duration', '-webkit-animation-duration'],
      // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
      // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
      overlay : false,
      overlayClass : 'animsition-overlay-slide',
      overlayParentElement : 'body',
      transition: function(url){ window.location.href = url; }
  });


  //reveal animation
  wow = new WOW(
    {
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       false,       // default
    live:         true        // default
  })

  wow.init();


  // Initiate the footer
  siteFooter();
  // could be simplified but I want to make it as easy to edit as possible
  $(window).resize(function() {
    siteFooter();
  });
  
  function siteFooter() {
    var siteContent = $('main');
    var siteContentHeight = siteContent.height();
    var siteContentWidth = siteContent.width();

    var siteFooter = $('.footer');
    var siteFooterHeight = siteFooter.height();
    var siteFooterWidth = siteFooter.width();

    // console.log('Content Height = ' + siteContentHeight + 'px');
    // console.log('Content Width = ' + siteContentWidth + 'px');
    // console.log('Footer Height = ' + siteFooterHeight + 'px');
    // console.log('Footer Width = ' + siteFooterWidth + 'px');

    siteContent.css({
      "margin-bottom" : siteFooterHeight
    });
  };

});
