<?php 
	//layout type: Certifications repeater field group

	$link = get_sub_field('cert_link');
	$text = get_sub_field('cert_text');
 ?>

 <section class="cert padding--both">
 	<div class="wrap clearfix hpad">
 		<div class="tvwelvecol white--bg clearfix bx-shadow--purple bx-shadow cert__box center">

 			<div class="cert__text">
 				<?php echo $text; ?>
 			</div>
 		
			<?php if ($link) : ?>
 			<a target="_blank" href="<?php echo $link; ?>" class="btn btn--gradient cert__btn">Se certificeringer</a>
 			<?php endif; ?>
 		</div>
 	</div>
 </section>
