<?php 

	if (have_rows('offer') ) :
 ?>

 <section class="offer padding--both">
 	<div class="wrap hpad clearfix">
 		<h2 class="offer__title center">Vi tilbyder også</h2>
 		<?php while (have_rows('offer') ) : the_row(); 
 				$title = get_sub_field('offer_title');
 				$icon  = get_sub_field('offer_icon');
 				$link = get_sub_field('offer_link');
 		?>
 		<div class="offer__item fourcol center">
 			<a href="<?php echo $link; ?>">
 				<h3 class="offer__item--title"><?php echo $title; ?></h3>
 				<img src="<?php echo $icon['sizes']['thumbnail']; ?>">
 			</a>
 		</div>
 	<?php endwhile; ?>
 	</div>
 </section>
 <?php endif; ?>