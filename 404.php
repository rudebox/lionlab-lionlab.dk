<?php get_template_part('parts/header'); ?>

<main>

  <section class="purple--bg hero">
  	<div class="wrap hpad clearfix">
  		<div class="row">
		
			<div class="sixcol">
			    <h1 class="hero__title">Hello</h1>
			    <p>Is it me you're looking four oh four?</p>
			    <p>If it was, you can find me <a target="_blank" href="http://lionelrichie.com/"><u>here</u></a></p>

			    <a class="btn btn--gradient" href="<?php bloginfo('url'); ?>">Tilbage</a>
		    </div>

	        <div class="sixcol hero__img--404">
	          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/richie_404.png" alt="lionelrichie_404">
	        </div>
	        
        </div>

  	</div>
  </section>

  <section class="padding--both">
  	<div class="wrap hpad clearfix">
  		<div class="row">
	  		<div class="twelvecol">
	  			Siden du søgte efter kan ikke findes.
	  		</div>
  		</div>
  	</div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>