<footer id="footer" class="footer clearfix">
  <div class="footer__item center">
    <h4 class="footer__item--title"><?php echo the_field('footer_title', 'options'); ?></h4>
    <?php echo the_field('footer_info', 'options'); ?>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
