<?php
  //hero field group
  $img = get_sub_field('image');
  $text = get_sub_field('hero_text');
  $link = get_sub_field('hero_link');
  $title = get_sub_field('hero_title');
?>


<section class="hero purple--bg">
  <div class="wrap clearfix hpad">
      <div class="row">

        <div class="sixcol">
          <?php if ($title) : ?>
          <h1 class="hero__title"><?php echo $title; ?></h1>
          <?php else: ?>
          <h1 class="hero__title"><?php echo the_title(); ?></h1>
          <?php endif; ?>
          <?php echo $text; ?>
          <a href="<?php echo $link; ?>" class="btn btn--gradient">Se mere</a>
        </div>

        <div class="sixcol hero__img wow fadeInRight delay-3s">
          <img src="<?php echo $img; ?>" alt="hero">
        </div>
    </div>
  </div>
</section>
