<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M38QL5P');</script>

  <!-- Nom Nom cookies -->
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script>
  window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#ff792a",
        "text": "#ffffff"
      },
      "button": {
        "background": "",
        "text": "#ffffff"
      }
    },
    "theme": "classic",
    "content": {
      "message": "Ja, vi bruger småkager.",
      "dismiss": "Roger that!",
      "link": "Læs mere her",
      "href": "lionlab.dk/privatlivspolitik"
    }
  })});
</script>
</head>

<?php if (!is_front_page() ) : ?>
<body <?php body_class('animsition'); ?>>
<?php else: ?>
<body <?php body_class(); ?>>
<?php endif; ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M38QL5P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php if (is_front_page() ) : ?>
<div class="preloader__wrapper">
    <div class="preloader">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-gif.gif" alt="preloader">
    </div>
</div>
<?php endif; ?>

<header class="header" id="header">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo"
       href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span>
      <img class="nav-toggle__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/burger.png" alt="burger-menu">
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>
