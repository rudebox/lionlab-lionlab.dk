<?php get_template_part('parts/header'); ?>
<?php get_template_part('parts/page', 'header'); ?>

<main>

  <section class="padding--both home">
    <div class="wrap hpad clearfix">

      <?php
        $category_name = "cases";
        $category = get_category_by_slug( $category_name );
        $category_id = $category->term_id;

        $args = array('child_of' => $category_id);
        $categories = get_categories( $args );
     ?>

     <div class="cases__controls mixit_controls flex flex--wrap">
          <div class="cases__filter btn btn--hollow" data-filter="all"><span>Alle</span></div>
        <?php foreach($categories as $category) : ?>
          <div class="cases__filter btn btn--hollow" data-filter=".cat<?php echo $category->term_id;?>"><span><?php echo $category->name; ?></span></div> 
        <?php endforeach; ?>
    </div>


    <?php 
      // Query Arguments
      $args = array(
        'posts_per_page' => -1,
        'order' => 'DESC',
        'category__not_in' => 9,
      );

      // The Query
      $query = new WP_Query( $args );
    ?>

    <div class="mixit cases__row flex flex--wrap">

      <?php if ($query->have_posts()): ?>
        <?php while ($query->have_posts()): $query->the_post(); ?>

        <?php 
            $cats = get_the_category();
            $cat_string = "";

            foreach ($cats as $cat) {
              $cat_string .= " cat" . $cat->term_id ."";
            }
        ?>

        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog' );?>

        <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" class="<?php echo $class; ?> mix <?php echo $cat_string; ?> cases__post cases__post--home cases__post--blog fourcol" itemscope itemtype="http://schema.org/BlogPosting">
            
          <div class="cases__content-wrap">
            <header>
              <img src="<?php echo $thumb['0']; ?>" alt="logo">
            </header>

            <h4 class="cases__post--title">
                <?php the_title(); ?>
            </h4>
                        
            <div class="cases__post--excerpt">
              <?php echo the_excerpt(); ?>
            </div>

          </div>

        </a>

        <?php endwhile; 
         wp_reset_postdata(); 
        else: ?>

          <p>No posts here.</p>

      <?php endif; ?>

    </div>
    </div> 

  </section>

</main>

<?php get_template_part('parts/footer'); ?>