<?php

/* Do not remove this line. */
require_once('includes/scratch.php');




/*
 * scratch_meta() adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  // wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', false, false, false );

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), null, true );
  }

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true );

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/compiled/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/compiled/main.min.js'), true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


//defer JS
function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.min.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}

add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 ); 


/* Enable ACF Options Pages */

if(function_exists('acf_add_options_page')) {

  acf_add_options_page();
  acf_add_options_sub_page('Header');
  acf_add_options_sub_page('Generelt indhold');
  acf_add_options_sub_page('Sidebar');
  acf_add_options_sub_page('Footer');

}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' )   // main nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'main-nav', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0    // fallback function
  ));
} /* end scratch main nav */


// Move Yoast to bottom
function yoasttobottom() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


//remove emoji, embed scripts and styling for performance optimization
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


//allow svg uploads
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


// Custom excerpt length

function custom_excerpt_length( $length ) {
  return 20;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



// Custom excerpt text

function custom_excerpt_more( $more ) {
  return '&hellip;';
}

add_filter('excerpt_more', 'custom_excerpt_more');


//remove countrycode from phone
function get_formatted_phone($str) {
  
  // Remove +45
  $str = str_replace('+', '00', $str);

  // Only allow integers
  $str = preg_replace('/[^0-9]/s', '', $str);

  return $str;
}


// Image sizes
add_image_size('employee', 1000, 628, true);
add_image_size('blog', 400, 300, true);


//get proper title
function get_proper_title( $id = null  ) {
  
  if ( is_null($id) ) {
    global $post;
    $id = $post->ID;
  }

  $acf_title = get_field('page_title', $id);

  return ($acf_title) ? $acf_title : get_the_title( $id );
}


//hide content editor on page-template layouts 
add_action( 'admin_init', 'hide_editor' );

function hide_editor() {

  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

  if( !isset( $post_id ) ) return;

  $forside = get_the_title($post_id);
  $contact = get_the_title($post_id);
  $employees = get_the_title($post_id);
  $seo = get_the_title($post_id);
  $hjemmeside = get_the_title($post_id);
  $facebook = get_the_title($post_id);
  $bannerdesign = get_the_title($post_id);
  $adwords = get_the_title($post_id);

  if($forside == 'Forside' || $kontakt == 'Kontakt' || $employees == 'Medarbejdere' || $seo == 'SEO' || $hjemmeside == 'Hjemmeside' || $facebook == 'Facebook' || $bannerdesign == 'Bannerdesign' || $bannerdesign == 'Bannerdesign' || $adwords == 'Adwords'){ 

    remove_post_type_support('page', 'editor');

  }


  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'contact-template.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}

//Body classes
function lionlab_body_classes($classes) {

  // Browser specific classes based on user agent globals
  global $is_gecko, $is_IE, $is_opera, $is_safari, $is_chrome, $is_edge, $is_iphone, $post;

  if ($is_gecko)          $classes[] = 'is-gecko';
  elseif ($is_opera)      $classes[] = 'is-opera';
  elseif ($is_iphone)       $classes[] = 'is-iphone';
  elseif ($is_safari)     $classes[] = 'is-safari';
  elseif ($is_chrome)     $classes[] = 'is-chrome';
  elseif ($is_IE)         $classes[] = 'is-ie';
  elseif ($is_edge)       $classes[] = 'is-edge';
  else                $classes[] = 'is-unknown';

  if (!is_front_page())   $classes[] = 'is-not-home';

  // WPML language
  if (function_exists('icl_object_id')) $classes[] = 'wpml-' . ICL_LANGUAGE_CODE;

  // Add post/page slug if not present and template slug
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
    $classes[] = str_replace('.php', '', basename(get_page_template()));
  }
  
  // Remove unnecessary classes
  $home_id_class = 'page-id-' . get_option('page_on_front');
  $remove_classes = array(
    'page-template-default',
    $home_id_class
  );
  
  $classes = array_diff($classes, $remove_classes);
  
  return $classes;
}

add_filter('body_class', 'lionlab_body_classes');



// Clean up wordpres <head>
  remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
  remove_action('wp_head', 'wp_generator'); // remove wordpress version
  remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
  remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
  remove_action('wp_head', 'index_rel_link'); // remove link to index page
  remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)
  remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
  remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
  remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);


  //GF scripts in footer
  add_filter("gform_init_scripts_footer", "init_scripts");
    function init_scripts() {
    return true;
  }

  //gravity form styles in header
  add_action('wp_enqueue_scripts', function() {
      if (function_exists('gravity_form_enqueue_scripts')) {
          // newsletter subscription form
          gravity_form_enqueue_scripts(5);
      }
  });


  // Remove Query Strings from Static Resources

  function remove_cssjs_ver( $src ) {
   if( strpos( $src, '?ver=' ) )
   $src = remove_query_arg( 'ver', $src );
   return $src;
  }
  add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
  add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );


  //remove gutenberg css
  function custom_theme_assets() {
    wp_dequeue_style( 'wp-block-library' );
  }
  add_action( 'wp_enqueue_scripts', 'custom_theme_assets', 100 );


  //html compression

   class WP_HTML_Compression
  {
      // Settings
      protected $compress_css = true;
      protected $compress_js = true;
      protected $info_comment = true;
      protected $remove_comments = true;

      // Variables
      protected $html;
      public function __construct($html)
      {
       if (!empty($html))
       {
         $this->parseHTML($html);
       }
      }
      public function __toString()
      {
       return $this->html;
      }
      protected function bottomComment($raw, $compressed)
      {
       $raw = strlen($raw);
       $compressed = strlen($compressed);
       
       $savings = ($raw-$compressed) / $raw * 100;
       
       $savings = round($savings, 2);
       
       return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
      }
      protected function minifyHTML($html)
      {
       $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
       preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
       $overriding = false;
       $raw_tag = false;
       // Variable reused for output
       $html = '';
       foreach ($matches as $token)
       {
         $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
         
         $content = $token[0];
         
         if (is_null($tag))
         {
           if ( !empty($token['script']) )
           {
             $strip = $this->compress_js;
           }
           else if ( !empty($token['style']) )
           {
             $strip = $this->compress_css;
           }
           else if ($content == '<!--wp-html-compression no compression-->')
           {
             $overriding = !$overriding;
             
             // Don't print the comment
             continue;
           }
           else if ($this->remove_comments)
           {
             if (!$overriding && $raw_tag != 'textarea')
             {
               // Remove any HTML comments, except MSIE conditional comments
               $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
             }
           }
         }
         else
         {
           if ($tag == 'pre' || $tag == 'textarea')
           {
             $raw_tag = $tag;
           }
           else if ($tag == '/pre' || $tag == '/textarea')
           {
             $raw_tag = false;
           }
           else
           {
             if ($raw_tag || $overriding)
             {
               $strip = false;
             }
             else
             {
               $strip = true;
               
               // Remove any empty attributes, except:
               // action, alt, content, src
               $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
               
               // Remove any space before the end of self-closing XHTML tags
               // JavaScript excluded
               $content = str_replace(' />', '/>', $content);
             }
           }
         }
         
         if ($strip)
         {
           $content = $this->removeWhiteSpace($content);
         }
         
         $html .= $content;
       }
       
       return $html;
      }
       
      public function parseHTML($html)
      {
       $this->html = $this->minifyHTML($html);
       
       if ($this->info_comment)
       {
         $this->html .= "\n" . $this->bottomComment($html, $this->html);
       }
      }
      
      protected function removeWhiteSpace($str)
      {
       $str = str_replace("\t", ' ', $str);
       $str = str_replace("\n",  '', $str);
       $str = str_replace("\r",  '', $str);
       
       while (stristr($str, '  '))
       {
         $str = str_replace('  ', ' ', $str);
       }
        
       return $str;
      }
  }

  function wp_html_compression_finish($html)
  {
      return new WP_HTML_Compression($html);
  }

  function wp_html_compression_start()
  {
      ob_start('wp_html_compression_finish');
  }
  add_action('get_header', 'wp_html_compression_start');


/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
