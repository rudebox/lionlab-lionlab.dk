<?php
/**
 * Template Name: Kontakt
 */

 get_template_part('parts/header'); the_post(); 

 get_template_part('parts/page', 'header'); ?> 

<main>

  <section class="padding--both">
    <div class="wrap hpad clearfix">
        <div class="eightcol twocol-offset">
        <?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
        </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>