  <section class="cases is-slider padding--both">
    <div class="wrap hpad clearfix cases__container"> 

      <h2 class="cases__title center">Referencer</h2> 

      <div class="cases__track">

      <?php 

        	$args = array(
        	  'posts_per_page' => 9,
            'order' => 'DESC',
            'category__not_in' => 9,
            'category__in' => 4,
        	);

        	$query = new WP_Query($args);

      ?>

      <?php if ($query->have_posts()): ?>
        <?php while ($query->have_posts()): $query->the_post(); ?>

        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog' );?>

        
        <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" class="cases__post cases__post--page fourcol" itemscope itemtype="http://schema.org/BlogPosting">

          <div class="cases__content-wrap">

            <header>
              <img src="<?php echo $thumb['0']; ?>" alt="logo">
            </header>

            <h2 class="cases__post--title h4">
              <?php the_title(); ?>
            </h2>

            <div class="cases__post--excerpt">
              <?php echo the_excerpt(); ?>
            </div> 

          </div>

        </a>

        <?php wp_reset_postdata(); ?>

        <?php endwhile; else: ?>

          <p>No posts here.</p>

      <?php endif; ?>
    </div>

  </div>

</section>