<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}
?>

<?php
  //hero field group
  $img = get_field('blog_img', 'options');
  $text = get_field('blog_text', 'options');
?>

<section class="hero purple--bg">
  <div class="wrap clearfix hpad">
      <div class="row">

        <div class="sixcol">
          <h1 class="hero__title"><?php echo $title; ?></h1>
          <?php if ($text) : ?>
          <?php echo $text; ?>
      	  <?php endif; ?>

        </div>

		<?php if ($img) : ?>
	        <div class="sixcol hero__img">
	          <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
	        </div>
        <?php endif; ?>
    </div>
  </div>
</section>
